<?php

namespace Drupal\hypothesis\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Hypothesis module Configuration form.
 *
 * @package Drupal\hypothesis\Form
 */
class HypothesisForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hypothesis_configuration_form';
  }

  /**
   * Configuration form name of Hypothesis.
   *
   * {@inheritDoc}.
   */
  protected function getEditableConfigNames() {
    return [
      'hypothesis.settings',
    ];
  }

  /**
   * Function contains configuration form elements the Hypothesis module.
   *
   * {@inheritDoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('hypothesis.settings');

    // Setup default behavior setting options.
    $default_options = [
      'showHighlights' => $this->t('Highlights on'),
      'openSidebar' => $this->t('Sidebar open'),
    ];
    $default_behavior_settings = $config->get('behavior');
    $default_behavior = !empty($default_behavior_settings) ?
      $default_behavior_settings : [];

    // Default Behaviors.
    $form['defaults'] = [
      '#type' => 'details',
      '#title' => t('Default Settings'),
      '#open' => TRUE,
    ];

    $form['defaults']['behavior'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Customize Hypothesis defaults and behavior below'),
      '#options' => $default_options,
      '#default_value' => $default_behavior,
    ];

    // Visibility settings.
    $form['visibility'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility settings'),
      '#attached' => [
        'library' => [
          'hypothesis/hypothesis.js',
        ],
      ],
    ];

    // Per-path visibility.
    $form['visibility']['path'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'visibility',
      '#weight' => 0,
    ];

    $options = [
      HYPOTHESIS_VISIBILITY_NOTLISTED => $this->t('All pages except those listed'),
      HYPOTHESIS_VISIBILITY_LISTED => $this->t('Only the listed pages'),
    ];

    $args = [
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ];

    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", $args);
    $title = $this->t('Pages');
    $hypothesis_visibility = $config->get('hypothesis_visibility');

    $form['visibility']['path']['hypothesis_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Hypothesis on specific pages'),
      '#options' => $options,
      '#default_value' => isset($hypothesis_visibility) ?
      $hypothesis_visibility : HYPOTHESIS_VISIBILITY_NOTLISTED,
    ];

    $hypothesis_pages = $config->get('hypothesis_pages');
    $form['visibility']['path']['hypothesis_pages'] = [
      '#type' => 'textarea',
      '#title' => $title,
      '#title_dispaly' => 'invisible',
      '#default_value' => $hypothesis_pages ? $hypothesis_pages : '',
      '#description' => $description,
    ];

    $default_type_options = $config->get('hypothesis_types');
    $form['visibility']['node_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types'),
      '#group' => 'visibility',
      '#weight' => 5,
    ];
    $form['visibility']['node_type']['hypothesis_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Show Hypothesis for specific content types'),
      '#default_value' => $default_type_options ? $default_type_options : [],
      '#options' => node_type_get_names(),
      '#description' => $this->t('Show Hypothesis only on pages that display content of the given type(s). If you select no types, there will be no type-specific limitation.'),
    ];

    $default_role_options = $config->get('hypothesis_roles');
    $form['visibility']['role'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'visibility',
      '#weight' => 10,
    ];
    $form['visibility']['role']['hypothesis_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Show Hypothesis for specific roles'),
      '#default_value' => $default_role_options ? $default_role_options : [],
      '#options' => user_role_names(),
      '#description' => $this->t('Show Hypothesis only for the selected role(s). If you select no roles, Hypothesis will be visible to all users.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Hypothesis module config form values submit function.
   *
   * {@interitdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hypothesis.settings');

    $config->set('behavior', $form_state->getValue('behavior'))
      ->set('hypothesis_visibility', $form_state->getValue('hypothesis_visibility'))
      ->set('hypothesis_pages', $form_state->getValue('hypothesis_pages'))
      ->set('hypothesis_types', $form_state->getValue('hypothesis_types'))
      ->set('hypothesis_roles', $form_state->getValue('hypothesis_roles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
