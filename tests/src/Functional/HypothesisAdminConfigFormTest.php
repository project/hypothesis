<?php

namespace Drupal\Tests\hypothesis\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests output on hypothesis administrative page.
 *
 * @group hypothesis
 */
class HypothesisAdminConfigFormTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['node', 'hypothesis'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test user with permission to access the administrative toolbar.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A non-administrator user for this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $regularUser;

  /**
   * The hypothesis admin form.
   *
   * @var \Drupal\Core\Url
   */
  private $formPath;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // User to check with admin access.
    $this->adminUser = $this->drupalCreateUser(
      [
        'administer hypothesis settings',
        'access administration pages',
        'administer modules',
      ]
    );

    // User to check non-admin access.
    $this->regularUser = $this->drupalCreateUser(['access content'], 'test-user');

    $this->formPath = Url::fromRoute('hypothesis.settings');
  }

  /**
   * Tests that user has assess to configure the Hypothesis settings.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testHypothesisAdminConfigFormAccess() {
    // Assure we loaded settings with proper permissions.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->formPath);
    $this->assertSession()->statusCodeEquals(200);

    // Assure the regular user can not access the form.
    $this->drupalLogin($this->regularUser);
    $this->drupalGet($this->formPath);
    $this->assertSession()->statusCodeNotEquals(200);
  }

  /**
   * Tests that user configured the Hypothesis correctly.
   */
  public function testHypothesisAdminConfigurationForm() {
    $this->drupalLogin($this->adminUser);

    // Save the configurations.
    $values = [
      'hypothesis_pages' => 'node/*/edit',
    ];

    $this->drupalPostForm($this->formPath, $values, 'Save configuration');

    // Load the config form.
    $this->drupalGet($this->formPath);

    $hypothesis_pages = $this->assertSession()->fieldExists('hypothesis_pages')->getValue();

    $this->assertTrue($hypothesis_pages == 'node/*/edit');

  }

}
